import 'package:flutter/material.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey exportKey = GlobalKey();
  FilePickerCross? filePickerCross;
  String _fileString = '';
  Set<String?> lastFiles = {}; // Inicializado para evitar null.

  @override
  void initState() {
    super.initState();
    _listInternalFiles(); // Usando a função personalizada para listar arquivos.
  }

  // Função personalizada para listar arquivos
  Future<void> _listInternalFiles() async {
    // Obtém o diretório de documentos (você pode ajustar para outro diretório se necessário)
    final directory = await getApplicationDocumentsDirectory();
    final files = Directory(directory.path).listSync(); // Lista os arquivos
    setState(() {
      lastFiles = files
          .map((file) => file.path)
          .toSet(); // Armazena os caminhos dos arquivos
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch().copyWith(
              primary: Colors.blueGrey, secondary: Colors.lightGreen)),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: ListView(
          padding: const EdgeInsets.all(8),
          children: <Widget>[
            Text(
              'Last files',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            (lastFiles.isEmpty)
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return ListTile(
                        leading: Text('$index.'),
                        title: Text(lastFiles.toList()[index]!),
                        onTap: () async {
                          // Usando o método correto para importar o arquivo de armazenamento
                          final filePicker =
                              await FilePickerCross.importFromStorage();
                          setFilePicker(filePicker);
                        },
                      );
                    },
                    itemCount: lastFiles.length,
                  ),
            Builder(
              builder: (context) => ElevatedButton(
                onPressed: () => _selectFile(context),
                child: const Text('Open File...'),
              ),
            ),
            (filePickerCross == null)
                ? const Text('Open a file first, to save')
                : ElevatedButton(
                    key: exportKey,
                    onPressed: _selectSaveFile,
                    child: const Text('Save as...'),
                  ),
            Text(
              'File details',
              style: Theme.of(context).textTheme.headlineLarge,
            ),
            Text(
                'File path: ${filePickerCross?.path ?? 'unknown'} (Might cause issues on web)\n'),
            Text('File length: ${filePickerCross?.length ?? 0}\n'),
            Text('File as String: $_fileString\n'),
          ],
        ),
      ),
    );
  }


void _selectFile(BuildContext context) {
  FilePickerCross.importFromStorage().then((result) {
    if (result != null) {
      // Verifica se o retorno é uma lista de arquivos
      if (result is List<FilePickerCross>) {
        // Aqui, o retorno é uma lista de arquivos
        if (result.length > 0) {
          setFilePicker(result); // Pega o primeiro arquivo da lista
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('You selected ${result.length} file(s).'),
            ),
          );
        } else {
          // Caso contrário, se a lista estiver vazia
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('No files selected.'),
            ),
          );
        }
      } else {
        // Se o retorno for um único arquivo, não uma lista
        setFilePicker(result); // Pega o único arquivo
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('You selected 1 file.'),
          ),
        );
      }

      setState(() {});
    } else {
      // Se o retorno for nulo
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('No file selected.'),
        ),
      );
    }
  }).catchError((error) {
    // Caso algum erro aconteça
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Error selecting file(s): $error'),
      ),
    );
  });
}






  void _selectSaveFile() {
    RenderBox renderBox =
        exportKey.currentContext!.findRenderObject() as RenderBox;
    Offset position = renderBox.localToGlobal(Offset.zero);
    filePickerCross!.exportToStorage(
        subject: filePickerCross!.fileName,
        sharePositionOrigin: Rect.fromLTWH(position.dx, position.dy,
            renderBox.size.width, renderBox.size.height));
  }

  setFilePicker(FilePickerCross filePicker) => setState(() {
        filePickerCross = filePicker;
        filePickerCross!.saveToPath(path: filePickerCross!.fileName!);
        lastFiles.add(filePickerCross!.fileName);
        try {
          _fileString = filePickerCross.toString();
        } catch (e) {
          _fileString = 'Not a text file. Showing base64.\n\n' +
              filePickerCross!.toBase64();
        }
      });
}
