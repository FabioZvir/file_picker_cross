import 'dart:typed_data';
import 'package:file_picker/file_picker.dart';

/// Helper function to parse the file extension
List<String>? parseExtension(String fileExtension) {
  return fileExtension.isNotEmpty ? [fileExtension] : null;
}

/// Implementation of file selection dialog using file_picker for desktop platforms
Future<Map<String, Uint8List>> selectFilesDesktop({
  required FileType type,
  required String fileExtension,
}) async {
  final FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: type,
    allowedExtensions: parseExtension(fileExtension),
  );

  if (result != null && result.files.isNotEmpty) {
    Map<String, Uint8List> fileBytes = {};
    for (var file in result.files) {
      fileBytes[file.name] = file.bytes!;
    }
    return fileBytes;
  } else {
    return {};
  }
}

/// Implementation of file selection dialog for multiple files using file_picker for desktop platforms
Future<Map<String, Uint8List>> selectMultipleFilesDesktop({
  required FileType type,
  required String fileExtension,
}) async {
  final FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: type,
    allowMultiple: true,
    allowedExtensions: parseExtension(fileExtension),
  );

  if (result != null && result.files.isNotEmpty) {
    Map<String, Uint8List> fileBytes = {};
    for (var file in result.files) {
      fileBytes[file.name] = file.bytes!;
    }
    return fileBytes;
  } else {
    return {};
  }
}

/// Implementation of file saving dialog using file_picker for desktop platforms
Future<String?> saveFileDesktop({
  required String fileExtension,
  String? suggestedFileName,
}) async {
  final String? savePath = await FilePicker.platform.saveFile(
    dialogTitle: 'Save your file',
    fileName: suggestedFileName ?? 'untitled.$fileExtension',
    type: FileType.custom,
    allowedExtensions: parseExtension(fileExtension),
  );

  return savePath;
}
