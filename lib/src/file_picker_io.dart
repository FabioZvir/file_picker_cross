import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:disk_space_plus/disk_space_plus.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';// Importar para XFile

import 'package:file_picker_cross/file_picker_cross.dart';
import 'file_picker_desktop.dart';
import 'file_picker_mobile.dart';

// Atualize a função para selecionar um único arquivo como bytes
Future<Map<String, Uint8List>> selectSingleFileAsBytes({
  required FileTypeCross type,
  required String fileExtension,
}) async {
  if (Platform.isAndroid || Platform.isIOS || Platform.isFuchsia) {
    return await selectFilesMobile(type: type, fileExtension: fileExtension);
  } else {
    return await selectFilesDesktop(
      type: fileTypeCrossParse(type), // Converta FileTypeCross para FileType
      fileExtension: fileExtension,
    );
  }
}

// Atualize a função para selecionar múltiplos arquivos como bytes
Future<Map<String, Uint8List>> selectMultipleFilesAsBytes({
  required FileTypeCross type,
  required String fileExtension,
}) async {
  if (Platform.isAndroid || Platform.isIOS || Platform.isFuchsia) {
    return await selectMultipleFilesMobile(
      type: type,
      fileExtension: fileExtension,
    );
  } else {
    return await selectMultipleFilesDesktop(
      type: fileTypeCrossParse(type), // Converta FileTypeCross para FileType
      fileExtension: fileExtension,
    );
  }
}

// Atualize a função para salvar um arquivo
Future<String?> pickSingleFileAsPath({
  required FileTypeCross type,
  required String fileExtension,
}) async {
  if (Platform.isAndroid || Platform.isIOS || Platform.isFuchsia) {
    return await saveFileMobile(type: type, fileExtension: fileExtension);
  } else {
    return await saveFileDesktop(
      fileExtension: fileExtension,
      suggestedFileName: null,
    );
  }
}

// Atualize a função para exportar para armazenamento externo
Future<String?> exportToExternalStorage({
  required Uint8List bytes,
  required String fileName,
  String? subject,
  String? text,
  Rect? sharePositionOrigin,
  bool share = true,
}) async {
  String extension = '.txt';

  if (fileName.contains('.')) {
    extension = fileName.substring(fileName.lastIndexOf('.'));
  }

  if (Platform.isAndroid || Platform.isIOS) {
    final String path = (await getTemporaryDirectory()).path + '/' + fileName;

    await File(path).writeAsBytes(bytes);

    if (Platform.isIOS) {
      // Para iOS, utilize `shareXFiles`
      await Share.shareXFiles(
        [XFile(path)],
        subject: subject,
        text: text,
        sharePositionOrigin: sharePositionOrigin,
      );
    } else {
      await FlutterFileDialog.saveFile(
        params: SaveFileDialogParams(sourceFilePath: path),
      );
    }

    return fileName;
  } else if (Platform.isWindows || Platform.isMacOS || Platform.isLinux) {
    String? path = await saveFileDesktop(
      fileExtension: extension,
      suggestedFileName: fileName,
    );

    if (path != null) {
      File file = await File(path).create(recursive: true);
      await file.writeAsBytes(bytes);
    }

    return path;
  } else {
    throw UnimplementedError(
      'Exportar arquivos não está implementado para esta plataforma.',
    );
  }
}

// Atualize a função para deletar um caminho interno
Future<bool> deleteInternalPath({
  required String path,
}) async {
  if (await fileByPath(path).exists()) {
    return fileByPath(path)
        .delete()
        .then((_) => true)
        .catchError((_) => false);
  } else {
    return true;
  }
}

// Atualize a função para obter a cota interna
Future<FileQuotaCross> getInternalQuota() async {
  double freeSpace = (await DiskSpacePlus.getFreeDiskSpace ?? 0) * 1e6;
  double totalSpace = (await DiskSpacePlus.getTotalDiskSpace ?? 0) * 1e6;
  return FileQuotaCross(
    quota: totalSpace.round(),
    usage: (totalSpace - freeSpace).round(),
  );
}

// Atualize a função para listar arquivos
Future<List<String>> listFiles({
  Pattern? at,
  Pattern? name,
}) async {
  final String appPath = await normalizedApplicationDocumentsPath();

  List<String> files = await Directory(appPath)
      .list(recursive: true)
      .map((event) => '/' + event.path.replaceFirst(appPath, ''))
      .toList();

  if (at != null) {
    files = files.where((element) => element.startsWith(at)).toList();
  }
  if (name != null) {
    files = files.where((element) => element.contains(name)).toList();
  }
  return files;
}

// Atualize a função para analisar extensões de arquivos
dynamic parseExtension(String fileExtension) {
  return (fileExtension
          .replaceAll(',', '')
          .trim()
          .replaceAll('.', '') // Remove o ponto inicial
          .isNotEmpty)
      ? fileExtension
          .split(',')
          .map<String>((e) => e.trim().replaceAll(".", ""))
          .toList()
      : null;
}

// Atualize a função para converter FileTypeCross para FileType
FileType fileTypeCrossParse(FileTypeCross type) {
  FileType accept;
  switch (type) {
    case FileTypeCross.any:
      accept = FileType.any;
      break;
    case FileTypeCross.audio:
      accept = FileType.audio;
      break;
    case FileTypeCross.image:
      accept = FileType.image;
      break;
    case FileTypeCross.video:
      accept = FileType.video;
      break;
    case FileTypeCross.custom:
      accept = FileType.custom;
      break;
  }
  return accept;
}

// Atualize a função para obter o caminho do diretório de documentos do aplicativo
Future<String> normalizedApplicationDocumentsPath() async {
  String directoryPath;

  String appName;
  try {
    appName = (await PackageInfo.fromPlatform()).appName;
  } catch (e) {
    appName = 'file_picker_cross';
  }

  if (Platform.isWindows) {
    directoryPath = Directory(r'%LOCALAPPDATA%' + r'/' + appName).path;
  } else {
    directoryPath = (await getApplicationDocumentsDirectory()).path;
  }
  String path = (directoryPath.replaceAll(r'\', r'/') + '/$appName/')
      .replaceAll(r'//', '/');
  if (!await Directory(path).exists()) {
    Directory(path).createSync(recursive: true);
  }
  return path;
}

// Atualize a função para obter um arquivo pelo caminho
File fileByPath(String path) => File.fromUri(Uri.file(path));
