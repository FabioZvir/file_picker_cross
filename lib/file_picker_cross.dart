import 'dart:convert';
import 'dart:typed_data';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:file_picker/file_picker.dart'; // Importando o pacote

class FilePickerCross {
  final FileTypeCross type;
  final String fileExtension;
  final String? path;
  final Uint8List _bytes;

  FilePickerCross(
    this._bytes, {
    this.path,
    this.type = FileTypeCross.any,
    this.fileExtension = '',
  });

  static Future<FilePickerCross> importFromStorage({
    FileTypeCross type = FileTypeCross.any,
    String fileExtension = '',
  }) async {
    try {
      final Map<String, Uint8List> file = await selectSingleFileAsBytes(
        type: type,
        fileExtension: fileExtension,
      );

      String _path = file.keys.toList()[0];
      Uint8List? _bytes = file[_path];

      if (_bytes == null) {
        throw FileSelectionCanceledError("Nenhum arquivo foi selecionado");
      }
      return FilePickerCross(_bytes,
          path: _path, fileExtension: fileExtension, type: type);
    } catch (e) {
      throw FileSelectionCanceledError(e);
    }
  }

  static Future<Map<String, Uint8List>> selectSingleFileAsBytes({
    FileTypeCross type = FileTypeCross.any,
    String fileExtension = '',
  }) async {
    final result = await FilePicker.platform.pickFiles(
      type: type == FileTypeCross.any
          ? FileType.any
          : (type == FileTypeCross.image
              ? FileType.image
              : (type == FileTypeCross.video
                  ? FileType.video
                  : FileType.custom)),
      allowedExtensions: fileExtension.isNotEmpty ? [fileExtension] : null,
    );

    if (result == null || result.files.isEmpty) {
      throw Exception("Nenhum arquivo selecionado.");
    }

    final filePath = result.files.single.path;
    final bytes = await File(filePath!).readAsBytes();

    return {filePath: bytes};
  }

  Future<Uint8List> internalFileByPath({required String path}) async {
    return await getFileBytesByPath(path);
  }

  Future<bool> saveInternalBytes({
    required Uint8List bytes,
    required String path,
  }) async {
    return await saveBytesToInternalPath(bytes, path);
  }

  Future<bool> saveToPath({
    required String path,
  }) {
    return saveInternalBytes(bytes: toUint8List(), path: path);
  }

  String? get fileName {
    final parsedPath = '/' + path!.replaceAll(r'\', r'/');
    return parsedPath.substring(parsedPath.lastIndexOf('/') + 1);
  }

  String get directory {
    final parsedPath = '/' + path!.replaceAll(r'\', r'/');
    return parsedPath.substring(0, parsedPath.lastIndexOf('/'));
  }

  @override
  String toString() => const Utf8Codec().decode(_bytes);

  Uint8List toUint8List() => _bytes;

  String toBase64() => base64.encode(_bytes);

  http.MultipartFile toMultipartFile({String? filename}) {
    filename ??= fileName;

    return http.MultipartFile.fromBytes(
      'file',
      _bytes,
      contentType: MediaType('application', 'octet-stream'),
      filename: filename,
    );
  }

  int get length => _bytes.lengthInBytes;
}

enum FileTypeCross { image, video, audio, any, custom }

class FileQuotaCross {
  final int quota;
  final int usage;

  FileQuotaCross({
    required this.quota,
    required this.usage,
  });

  int get remaining => quota - usage;

  double get relative => usage / quota;

  @override
  String toString() {
    return 'instance of FileQuotaCross{ quota: $quota, usage: $usage }';
  }
}

class FileSelectionCanceledError implements Exception {
  late dynamic _msg;

  FileSelectionCanceledError([dynamic msg = '']) {
    _msg = msg;
  }

  String reason() {
    String _err = _msg.toString();
    String _methodMap(String exceptionType) {
      String _reasonCollector;

      switch (exceptionType) {
        case 'RangeError (index)':
        case 'NoSuchMethodError':
          _reasonCollector = 'seleção_cancelada';
          break;

        default:
          _reasonCollector = '';
          break;
      }

      return _reasonCollector;
    }

    String _reasonResult = '';
    String _exception = _err.split(':')[0];
    String _methodData = _methodMap(_exception);

    if (_methodData != '') {
      _reasonResult = _methodData;
    } else {
      _reasonResult = _msg;
    }

    return _reasonResult;
  }

  @override
  String toString() => 'FileSelectionCanceledError: $_msg';
}

// Funções auxiliares
Future<Uint8List> getFileBytesByPath(String path) async {
  final file = File(path);
  return await file.readAsBytes();
}

Future<bool> saveBytesToInternalPath(Uint8List bytes, String path) async {
  final file = File(path);
  await file.writeAsBytes(bytes);
  return true;
}
